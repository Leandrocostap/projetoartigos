import React from 'react'
import styles from './Home.module.css'

const Home = () => {
  const [artigoName, setArtigoName] = React.useState('')
  const [artigo, setArtigo] = React.useState('')

  const [artigos, setArtigos] = React.useState([])


  return (
    <section className={styles.section}>

      <h1>Poste aqui seu Artigo ! </h1>

      <form action="" className={styles.form}>
        <div className={styles.artigo}>
          <label
            htmlFor="NameArtigo"
            className={styles.label}
          >
            Nome do artigo
          </label>
          <input
            type="text"
            name='NameArtigo'
            id='NameArtigo'
            className={styles.input}
            value={artigoName}
            onChange={({ target }) => setArtigoName(target.value)}
          />
        </div>
        <div className={styles.artigo}>
          <label htmlFor="conteudo" className={styles.label}>Escreva o artigo</label>
          <textarea
            name="conteudo"
            id="conteudo"
            cols="30"
            rows="10"
            className={styles.textArea}
            value={artigo}
            onChange={({ target }) => setArtigo(target.value)}
          />
        </div>
        <button
          className={styles.button}
          onClick={(event) => {
            event.preventDefault()
            setArtigos([...artigos, { artigoName, artigo }])
          }}
        >
          Postar
        </button>
      </form>
      <article className={styles.article}>
        {artigos && artigos.map((item, index) => {
          return (
            <div key={index} className={styles.artigoContainer}>
              <div className={styles.artigoContent}>
                <h2>{item.artigoName}</h2>
                <p>{item.artigo}</p>
              </div>
              <div className={styles.lixeira}>
                <button
                  onClick={() => {
                    const newArray = [...artigos]
                    newArray.splice(index, 1)
                    setArtigos(newArray)
                  }}
                >
                  <img src="lixeira.svg" alt="Lixeira" />
                </button>
              </div>
            </div>
          )
        })}
      </article>
    </section>
  )
}

export default Home
